"use strict"
const axios = require('axios')

async function getRest (param) {
  try {
    const cell = await axios.get(`https://api.clue.io/api/profiles/count?where={"cell_id":"${param.CL_Name}"}&user_key=126380548b6061d136ab4b1c2555ee73`)
    return cell
  } catch (error) {
    console.error(error)
  }
}


module.exports = {
  getRest: getRest
};

