"use strict"
const express = require('express')
const router = express.Router()
const Matrix = require('../models/matrix')
const Metadata = require('../models/metadata')
const Annotation = require('../models/annotation')
const asyncs = require('async')
const csv = require("fast-csv")
const { getRest } = require('./helper')
const fs = require('fs')
const archiver = require('archiver')
const _ = require('underscore')
const { PCA } = require('ml-pca')
const math = require('mathjs')

router.use(function(req, res, next) {
    console.log('Loading...');
    next();
});

router.get('/tags', async (req, res, next) => {
  try {
    const distinctCLName = await Metadata.distinct('CL_Name');
    const distinctSMName = await Metadata.distinct('SM_Name');
    const distinctSMDose = await Metadata.distinct('SM_Dose');
    const distinctSMTime = await Metadata.distinct('SM_Time');

    const formatMetadata = (name, tags) => {
      return tags.map((tag) => ({ name, tag }));
    };

    const allOptions = [
      { metadata: 'Cell Line', metas: formatMetadata('CL_Name', distinctCLName) },
      { metadata: 'Compound/Drugs', metas: formatMetadata('SM_Name', distinctSMName) },
      { metadata: 'Dosage', metas: formatMetadata('SM_Dose', distinctSMDose.map((tag) => tag + 'um')) },
      { metadata: 'Time Point', metas: formatMetadata('SM_Time', distinctSMTime.map((tag) => tag + 'h')) },
    ];

    res.json(allOptions);
  } catch (err) {
    next(err);
  }
});

router.post('/search', async (req, res, next) => {
  try {
    const { values, page, limit } = req.body;

    const fields = { $and: [] };
    const categories = { SM_Dose: [], SM_Time: [], CL_Name: [], SM_Name: [] };

    for (const d of values) {
      const tag = d.name === 'SM_Dose' ? d.tag.slice(0, -2) : d.name === 'SM_Time' ? d.tag.slice(0, -1) : d.tag;
      const valuesObj = { [d.name]: tag };
      categories[d.name].push(valuesObj);
    }

    for (const [name, values] of Object.entries(categories)) {
      if (values.length > 0) fields.$and.push({ $or: values });
    }

    const options = { page, limit, sort: {} }; 
    const model = await Metadata.paginate(fields, options);

    res.json({ model });
  } catch (error) {
    next(error); 
  }
});

// Download

router.get('/download/:ids', (req, res, next) => {

  let filename = 'results.zip'
  res.setHeader('Content-disposition', 'attachment; filename=results.zip')
  res.setHeader('content-type', 'application/zip')

  let archive = archiver('zip', {
      store: true
  })

  archive.on('error', function(err) {
    throw err
  })

  let data = req.params.ids.split(',')
  let params = data.shift()

  let landmark = params

  let queries = []
  queries.push(function (cb) {
    Annotation.find({}, {_id: 0, pr_gene_symbol: 1}).then( (results) => {
        cb(null, results)
    })
  })

  queries.push(function (cb) {
    Matrix.find({id: { $in: data }}, {_id: 0}).then( (results) => {
        cb(null, results)
    })
  })

  asyncs.parallel(queries, function(err, results) {
    if (err) {
        throw err
    }

    let csvStream = csv.createWriteStream({
      headers: true,
      objectMode: true,
      quote: ' '
    })

    let csvStreamMeta = csv.createWriteStream({
      headers: true,
      objectMode: true,
      quote: ' '
    })

    let meta = [
      "id",
      "CL_Name",
      "SM_Center_Compound_ID",
      "SM_Dose",
      "SM_Dose_Unit",
      "SM_LINCS_ID",
      "SM_Name",
      "SM_Pert_Type",
      "SM_Time",
      "SM_Time_Unit",
      "det_plate",
      "det_well"
    ]
    let headers = ['gene_symbols', data]

    csvStream.write(headers)
    csvStreamMeta.write(meta)

    let symbols = results[0].map(function(s) { return s.pr_gene_symbol })

    let matrixData = []

    // Not landmark
    if(landmark === 'false'){
        matrixData.push(symbols)
        results[1].forEach( (r) => {
          matrixData.push(r.vector)

          csvStreamMeta.write([
            [r.id], [r.CL_Name], [r.SM_Center_Compound_ID], [r.SM_Dose], [r.SM_Dose_Unit],
            [r.SM_LINCS_ID], [r.SM_Name], [r.SM_Pert_Type], [r.SM_Time], [r.SM_Time_Unit],
            [r.det_plate], [r.det_well]
          ])
        })
    }
    if(landmark === 'true'){
        matrixData.push(symbols.slice(0, 978))
        results[1].forEach( (r) => {
          matrixData.push(r.vector.slice(0, 978))

          csvStreamMeta.write([
            [r.id], [r.CL_Name], [r.SM_Center_Compound_ID], [r.SM_Dose], [r.SM_Dose_Unit],
            [r.SM_LINCS_ID], [r.SM_Name], [r.SM_Pert_Type], [r.SM_Time], [r.SM_Time_Unit],
            [r.det_plate], [r.det_well]
          ])
        })
    }

    let matrix = _.zip.apply(_,matrixData)

    matrix.forEach( (r) => {
      csvStream.write(r)
    })

    csvStream.end()
    csvStreamMeta.end()

    archive.append(csvStream, { name: 'matrix.csv' })
    archive.append(csvStreamMeta, { name: 'metadata.csv' })
    archive.finalize()
    archive.pipe(res)
  })
})

// Plots

router.post('/plot',  async (req, res, next) => {

  let data = req.body.values

  let queries = []

  try {
      const annot = await Annotation.find({"pr_is_bing": 1}, {_id: 0, pr_gene_symbol: 1}).exec()
      const matrx = await Matrix.find({id: { $in: data }}, {_id: 0}).exec()

      let meta = [
          "id",
          "CL_Name",
          "SM_Center_Compound_ID",
          "SM_Dose",
          "SM_Dose_Unit",
          "SM_LINCS_ID",
          "SM_Name",
          "SM_Pert_Type",
          "SM_Time",
          "SM_Time_Unit",
          "det_plate",
          "det_well"
        ]

      let symbols = annot.map(function(s) { return s.pr_gene_symbol })

      let matrixData = []
      let metaData = []
      matrixData.push(symbols)

      matrx.forEach( (r) => {
          matrixData.push(r.vector.slice(0, 978))

          let vec = [
            r.id, r.CL_Name, r.SM_Center_Compound_ID, r.SM_Dose, r.SM_Dose_Unit,
            r.SM_LINCS_ID, r.SM_Name, r.SM_Pert_Type, r.SM_Time, r.SM_Time_Unit,
            r.det_plate, r.det_well
          ]

          let mapping = Object.assign.apply({}, meta.map( (v, i) => ( {[v]: vec[i]} ) ) )
          metaData.push(mapping)
        })

      res.json({"matrix" : matrixData, "metadata": metaData, "selected": data})

  } catch (err) {
        return next(err)
  }

})

router.post('/plotpca',  async (req, res, next) => {

  let data = req.body.values
  const matrx = await Matrix.find({id: { $in: data }}, {_id: 0}).exec()

  let metaData = []
  let matrixData = []

  matrx.forEach( (r) => {
    matrixData.push(r.vector.slice(0, 978))
    metaData.push(r.SM_Pert_Type)
  })

  const pca = new PCA(math.transpose(matrixData))

  res.json({
    pcaVar  : pca.getExplainedVariance(),
    pcaCum  : pca.getCumulativeVariance(),
    pcaLoad : pca.getLoadings(),
    types: metaData
  })

})




module.exports = router
