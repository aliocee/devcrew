import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home'
import NProgress from 'nprogress'


Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/', name: 'Home', component: Home },
    { path: '/visualize', name: 'Visualize', component: require('../views/search/visualize.vue') },
    { path: '/plot', name: 'Plots', component: require('../views/search/plot.vue') },
    { path: '/guide', name: 'Guide', component: require('../views/search/guide.vue') },
    { path: '/contact', name: 'Contact', component: require('../views/search/contact.vue') }
  ]
})

router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
      // Start the route progress bar.
      NProgress.start()
  }
  next()
})

router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
  NProgress.done()
})

export default router

